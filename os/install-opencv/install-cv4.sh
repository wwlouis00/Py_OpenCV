
# https://www.pyimagesearch.com/2018/08/15/how-to-install-opencv-4-on-ubuntu/

sudo apt-get update -y
sudo apt-get upgrade -y

# developer tools
sudo apt-get install build-essential cmake unzip pkg-config -y

# image and video I/O libraries
sudo apt-get install libjpeg-dev libpng-dev libtiff-dev -y
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev -y
sudo apt-get install libxvidcore-dev libx264-dev -y

# GTK
sudo apt-get install libgtk-3-dev -y

# mathematical optimizations for OpenCV
sudo apt-get install libatlas-base-dev gfortran -y

# Python 3 development headers
sudo apt-get install python3-dev -y

# download :  opencv  and opencv_contrib
cd ~
# wget -O opencv.zip https://github.com/opencv/opencv/archive/4.0.0.zip
# wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/4.0.0.zip
# cp  /home/ssd/下載/opencv.zip  .
# cp  /home/ssd/下載/opencv_contrib.zip  .
# cp  /home/ssd/下載/get-pip.py  .

unzip opencv.zip || exit $?

unzip opencv_contrib.zip || exit $?


mv opencv-4.0.0 opencv
mv opencv_contrib-4.0.0 opencv_contrib

# install pip
# wget https://bootstrap.pypa.io/get-pip.py
sudo python3 get-pip.py  || exit $?

# install   virtualenv  and virtualenvwrapper
sudo pip install virtualenv virtualenvwrapper
sudo rm -rf ~/get-pip.py ~/.cache/pip


# virtualenv and virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source /usr/local/bin/virtualenvwrapper.sh


echo -e "\n# virtualenv and virtualenvwrapper" >> ~/.bashrc
echo "export WORKON_HOME=$HOME/.virtualenvs" >> ~/.bashrc
echo "export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3" >> ~/.bashrc
echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc
# source ~/.bashrc

# create your OpenCV 4 + Python 3 virtual environment:
mkvirtualenv cv -p python3

# verify  the cv  environment
workon cv

# install NumPy
pip install numpy

cd ~/opencv
mkdir build
cd build

# configure the OpenCV 4 build
cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D CMAKE_INSTALL_PREFIX=/usr/local \
	-D INSTALL_PYTHON_EXAMPLES=ON \
	-D INSTALL_C_EXAMPLES=OFF \
	-D OPENCV_ENABLE_NONFREE=ON \
	-D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules \
	-D PYTHON_EXECUTABLE=~/.virtualenvs/cv/bin/python \
	-D BUILD_EXAMPLES=ON ..
	
#  結束前 有這幾行
#--   Python 3:
#--     Interpreter:             /home/ssd/.virtualenvs/cv/bin/python3 (ver 3.5.2)
#--     Libraries:                   /usr/lib/x86_64-linux-gnu/libpython3.5m.so (ver 3.5.2)
#--     numpy:                       /home/ssd/.virtualenvs/cv/lib/python3.5/site-packages/numpy/core/include (ver 1.15.4)
#--     packages path:        lib/python3.5/site-packages



# 看 cpu 幾核
cat /proc/cpuinfo

date 
# 4核  大約  30 分鐘
#  -j4 用4核， -j2 用2核
make -j4
	
date 

# install OpenCV4
sudo make install
sudo ldconfig


workon cv
python --version
# 顯示 Python 3.5.2

# create a symbolic link : 
cd ~/.virtualenvs/cv/lib/python3.5/site-packages/    || exit $?
ln -s /usr/local/python/cv2 cv2
cd ~


workon cv
python
# 在 python 裡， 
# 顯示 >>>
import cv2
cv2.__version__
# 顯示  '4.0.0'
quit()
# 離開 python ，回到 bash 


workon cv
pip install imutils



	



