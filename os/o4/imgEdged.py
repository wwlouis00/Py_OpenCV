from pyimagesearch.transform import four_point_transform
from skimage.filters import threshold_local
import numpy as np
import argparse
import cv2
import imutils

image = cv2.imread( 'flower.jpg' )

ratio = image.shape[0] / 500.0
orig = image.copy()
image = imutils.resize(image, height = 500)

cv2.putText(image, "iot13", (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0, 255, 0), 2)

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
gray = cv2.GaussianBlur(gray, (5, 5), 0)
edged = cv2.Canny(gray, 75, 100)

cv2.imshow("Image", image)
cv2.imshow("gray", gray)
cv2.imshow("E_75_100", edged)

cv2.imshow("E_100_200", cv2.Canny(gray, 100, 200))
cv2.imshow("E_30_200", cv2.Canny(gray, 30, 200))
cv2.imshow("E_30_75", cv2.Canny(gray, 30, 75))

cv2.waitKey(0)
cv2.destroyAllWindows()

cv2.imwrite( '02a.jpg', image )
cv2.imwrite( '02b.jpg', gray )
cv2.imwrite( '02c.jpg', edged )


