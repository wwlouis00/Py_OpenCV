# python tryarg.py  --image rooster.jpg  --prototxt  book  -m good 
# python tryarg.py  -i rooster.jpg  -p  book  -m good 
# python tryarg.py  -i rooster.jpg  -p  book
# python tryarg.py  -h

import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,	help="path to input image")
ap.add_argument("-p", "--prototxt", required=True,	help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,	help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.5,	help="minimum probability to filter weak detections")

aa = vars(ap.parse_args())
print( aa )
print( aa['image'] )


bb = ap.parse_args()
print( bb )
print( bb.image )
        
